'use strict';

const issuer   = 'https://dev-796435.okta.com/oauth2/default';
const clientID      = '0oaciys9qOxtKbEg54x6';

const serverPort    = 3000;

module.exports.serverPort = serverPort;

module.exports.credentials = {
    issuer: issuer,
    clientId: clientID
};

const connectionURL = 'mongodb://localhost:27017/'
const database = 'test'

module.exports.mongodbcredential = {
    connectionURL : connectionURL,
    database : database
}