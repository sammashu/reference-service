"use strict";

const OktaJwtVerifier = require('@okta/jwt-verifier');
var config = require("../config");

const oktaJwtVerifier = new OktaJwtVerifier(config.credentials);

function checkValidToken(req, res, msg) {
        oktaJwtVerifier.verifyAccessToken(req.token, "api://default")
            .then(jwt => {
                res.json(msg);
            })
            .catch(err => {
                res.json(err);
            })

}

function authenticate(req, res, next) {
    const bearerHeader = req.headers['authorization'];

    if (bearerHeader) {
        const bearer = bearerHeader.split(' ');
        const bearerToken = bearer[1];
        req.token = bearerToken;
        next();
    } else {
        // Forbidden
        res.status(403);
    }
}

module.exports = {
    checkValidToken,
    authenticate
}